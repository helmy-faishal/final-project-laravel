<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profile';
    protected $guarded = [];

    public function User()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
