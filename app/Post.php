<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Post extends Model
{
    protected $guarded = [];
    // protected $fillable = [
    //     'category_id', 'user_id', 'tittle','content','imagespath',
    // ];

    protected $table = 'posts';

    public function withUser()
    {
        return $this->belongsTo(User::class,'user_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }

    public function comment(){
        return $this->hasMany('App\Comment');
    }
}
