<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Category extends Model
{
    protected $table = "category";
    protected $guarded = [];

    public function posts(){
        return $this->hasMany('App\Post');
    }
    
    public function withUser()
    {
        return $this->belongsTo(User::class,'user_id', 'id');
    }
}
