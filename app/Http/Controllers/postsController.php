<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
// use App\Http\Controllers\Auth;
use App\Category;
use App\Comment;
use Auth;
use Alert;

class postsController extends Controller
{

    public function createpost()
    {
        $data['category'] = Category::all();
        return view('posts.createpost',compact('data'));
    }

    public function create(Request $request)
    {
        $request->validate(
            [
                'tittle' => 'required',
                'content' => 'required',
                'category' => 'required',
                'image' => 'image'
            ],
            [
                'tittle.required' => 'tittle harus diisi',
                'content.required'  => 'content harus diisi',
                'category' => 'category harus diisi'
            ]
        );

        if($request->file('image')){
            $image = $request->file('image')->store('post-images');
        }else{
            $image = null;
        }

            Post::create(
                [
                    'user_id' => auth()->user()->id,
                    'tittle' => $request['tittle'],
                    'content' => $request['content'],
                    'category_id' => $request['category'],
                    'imagespath' => $image

                ]
            );

            Alert::success('Success', 'Success add new post');

            // return redirect(route('home'));
            return redirect('/')->with('success','Success add new post');
    }


    public function show($id)
    {
        $data['posts'] = Post::where('id', $id)->first();
        $data['comments'] = Comment::where('posts_id',$id)->get();
        return view('posts.detail', compact('data'));
    }

    public function edit($id)
    {
        $data['posts'] = Post::where('id', $id)->first();
        $data['category'] = Category::all();

        return view('posts.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $request->validate(
            [
                'tittle' => 'required',
                'content' => 'required',
                'category' => 'required',
                'image' => 'image'
            ],
            [
                'tittle.required' => 'tittle harus diisi',
                'content.required'  => 'content harus diisi',
                'category' => 'category harus diisi'
            ]
        );

        if($request->file('image')){
            Post::where('id', $id)
            ->update(
                [
                    'user_id' => auth()->user()->id,
                    'tittle' => $request['tittle'],
                    'content' => $request['content'],
                    'category_id' => $request['category'],
                    'imagespath' =>$request->file('image')->store('post-images')
                ]
            );
        }else{
            Post::where('id', $id)
            ->update(
                [
                    'user_id' => auth()->user()->id,
                    'tittle' => $request['tittle'],
                    'content' => $request['content'],
                    'category_id' => $request['category']
                ]
            );
        }


        Alert::success('Success', 'Success edit post');

        // return redirect(route('home'));
        return redirect('/')->with('success','Success edit post');
    }

    public function delete($id){
        Post::where('id', '=', $id)->delete();

        Alert::success('Success', 'Success delete post');

        // return redirect(route('home'));

        return redirect('/')->with('success','Success delete post');
    }
}

