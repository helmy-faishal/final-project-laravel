<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Post;
use App\Comment;
use Illuminate\Support\Facades\DB;
use Auth;
use Alert;


class categoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }

    public function create()
    {
        return view('category.create');
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                'name' => 'required',
            ],
            [
                'name.required' => 'Category name must be filled',
            ]
        );
        DB::table('category')->insert(
            [
                'name' => $request['name'],
            ]
        );
            Alert::success('Success', 'Success add new category');

            // return redirect(route('home'));
            return redirect('/category')->with('success','Success add new category');
    }

    public function index ()
    {
        $category = DB::table('category')->get();

        return view('category.index', compact('category'));
    }

    public function show ($id)
    {
        $data['posts'] = Post::where('category_id',$id)->get();
        $data['category'] = Category::where('id',$id)->first();
        return view('category.show', compact('data'));
    }

    public function edit($id)
    {
        $category = DB::table('category')->where('id', $id)->first();
        // dd($cast);

        return view('category.edit', compact('category'));
    }

    public function update(Request $request, $id)
    {
        $request->validate(
            [
            'name' => 'required',
            ],
            [
                'name.required' => 'Category name must be filled',
            ]
        );

        DB::table('category')
              ->where('id', $id)
              ->update(
                  [
                      'name' => $request['name'],
                  ]
                );

        Alert::success('Success', 'Success update category');

            // return redirect(route('home'));
            return redirect('/category')->with('success','Success update category');
    }

    public function destroy($id)
    {
        $posts = Post::where('category_id',$id)->get();

        foreach ($posts as $key => $item) {
            $comments = Comment::where('posts_id',$item->id)->get();
            $comments->each->delete();
        }
        $posts->each->delete();

        Category::where('id', $id)->delete();

        Alert::success('Success', 'Success delete category');

            // return redirect(route('home'));
        return redirect('/category')->with('success','Success delete category');
    }

}
