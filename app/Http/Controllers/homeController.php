<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['home','search']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // return view('home');
        return redirect('/');
    }

    public function home()
    {
        $data['posts'] = Post::with('withUser')->get();

        return view('home.home', compact('data'));
    }

    public function search(Request $request){
        $query ='%' . $request['search'] . '%';
        $data['posts'] = Post::where('tittle','LIKE',$query)->get();
        
        return view('home.home', compact('data'))->with('search',$request['search']);
    }
}
