<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Auth;
use Alert;

class commentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        $request->validate([
            'message' => 'required',
        ]);

        $comment = Comment::create([
            'user_id' => Auth::user()->id,
            'posts_id' => $request['id_post'],
            'content' => $request['message']
        ]);

        Alert::success('Success', 'Success add comment');

        return redirect()->back()->with('success','success add comment');
    }

    public function edit($id){
        $data = Comment::find($id);
        return view('comment.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $request->validate([
            'content' => 'required'
        ]);

        Comment::where('id', $id)->update([
            'content' => $request['content']
        ]);
        
        $post_id = Comment::where('id', $id)->first()->posts_id;
            
        Alert::success('Success', 'Success edit comment');
        return redirect()->route('show', ['post_id' => $post_id])->with('success','Success edit comment');
    }

    public function destroy($id){
        $post_id = Comment::where('id', $id)->first()->posts_id;
        Comment::find($id)->delete();

        Alert::success('Success', 'Success delete comment');
        return redirect()->route('show', ['post_id' => $post_id])->with('success','Success delete comment');
    }
}