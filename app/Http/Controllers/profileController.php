<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\User;
use App\Post;
use App\Comment;
use Auth;
use Alert;

class profileController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $id = Auth::user()->id;
        $data['posts'] = Post::where('user_id',$id)->get();
        $data['comments'] = Comment::where('user_id',$id)->get();
        return view('profile.index',compact('data'));
    }
    public function edit(){
        $id = Auth::user()->id;
        $data = Profile::find($id);
        return view('profile.edit', compact('data'));
    }
    public function update(Request $request){
        $id = Auth::user()->id;
        $user = User::find($id);

        $request->validate([
            'name' => 'required',
            'age' => 'required|integer'
        ]);

        if ($request['name'] != $user->name){
            $user->name = $request['name'];
            $user->save();
        }

        $profile = Profile::where('id',$id)->update([
            'name' => $request['name'],
            'age' => $request['age'],
            'bio' => $request['bio'],
            'address' => $request['address']
        ]);

        Alert::success('Success', 'Success change profile');

        return redirect('/profile')->with('success','Success change profile');
    }
}
