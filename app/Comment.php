<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';
    protected $guarded = [];

    public function posts(){
        return $this->hasMany('App\Post');
    }

    public function user(){
        return $this->hasMany('App\User');
    }

}
