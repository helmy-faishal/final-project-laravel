# Final Project Laravel

# Kelompok 12

# Anggota Kelompok
- [Muhammad Helmy Faishal](https://gitlab.com/helmy-faishal)
- [Rheza maulana](https://gitlab.com/maulanarheza96)
- [Riko Susetia Yuda](https://gitlab.com/rikosusetiay)

# Tema Project
<p> Tema: <b>Forum Tanya Jawab</b> </p>

# ERD
<!-- ![ERD Forum Tanya Jawab](https://gitlab.com/helmy-faishal/final-project-laravel/-/raw/master/ERD%20Forum%20Tanya%20Jawab.png?inline=false) -->

![ERD Forum Tanya Jawab](https://gitlab.com/helmy-faishal/final-project-laravel/-/raw/master/ERD%20Forum%20Tanya%20Jawab%20Sanbercode.png?inline=false)

# Link Video
<p>Link Demo : https://www.youtube.com/watch?v=bzernDKTFIs </p>
<p> Link Deploy : https://quorum.sanbercodeapp.com/ </p>

