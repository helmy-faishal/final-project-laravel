@extends('layout.master');
@section('judul')
    Detail Post
@endsection
@section('isi')
    <main id="tt-pageContent">
        <div class="container">
            <div class="tt-single-topic-list">
                <div class="tt-item">
                    <div class="tt-single-topic">
                        <div class="tt-item-header">
                            <div class="tt-item-info info-top">
                                <div class="tt-avatar-icon">
                                    <i class="tt-icon"><svg>
                                            <use xlink:href="#icon-ava-{{strtolower($data['posts']->withUser->name[0])}}"></use>
                                        </svg></i>
                                </div>
                                <div class="tt-avatar-title">
                                    <a href="#">{{ $data['posts']->withUser->name }}</a>
                                </div>
                                <a href="#" class="tt-info-time">
                                    <i class="tt-icon"><svg>
                                            <use xlink:href="#icon-time"></use>
                                        </svg></i>{{ \Carbon\Carbon::CreateFromFormat('Y-m-d H:i:s', $data['posts']->created_at)->format('d F Y') }}
                                </a>
                            </div>
                            <h3 class="tt-item-title">
                                <div class="text-break">
                                    <a href="#">{{ $data['posts']->tittle }}</a>
                                </div>
                            </h3>
                            <div class="tt-item-tag">
                                <ul class="tt-list-badge">
                                    <li><a href="#"><span
                                                class="tt-color03 tt-badge">{{ $data['posts']->category->name }}</span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="tt-item-description">
                            <div class="text-center">
                                <img src="{{ asset('storage/' . $data['posts']->imagespath) }}"
                                    alt="{{ $data['posts']->category->name }}" class="img-fluid-mt-3 mb-4" width="50%">
                            </div>
                            <div class="text-break">
                                <p> {{ $data['posts']->content }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="/comment" method="post">
                    @csrf
                    @method('post')
                    <div class="tt-wrapper-inner">
                        <div class="pt-editor form-default">
                            <h6 class="pt-title">Post Your Reply</h6>
                            <div class="pt-row">
                            </div>
                            <div class="form-group">
                                <textarea name="message" class="form-control" rows="5"
                                    placeholder="Lets get started"></textarea>
                            </div>
                            <div class="pt-row justify-content-end">
                                <div class="col-auto">
                                    <input type="hidden" name="id_post" value="{{$data['posts']->id}}">
                                    <input type="submit" href="/comment" class="btn btn-secondary btn-width-lg">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <hr>

                @forelse ($data['comments'] as $comment)
                @php
                    $name = App\User::where('id',$comment->user_id)->first()->name
                @endphp
                    <div class="tt-item bg-light">
                        <div class="tt-single-topic">
                            <div class="tt-item-header pt-noborder">
                                <div class="tt-item-info info-top">
                                    <div class="tt-avatar-icon">
                                        <i class="tt-icon"><svg>
                                                <use xlink:href="#icon-ava-{{strtolower($name[0])}}"></use>
                                            </svg></i>
                                    </div>
                                    <div class="tt-avatar-title">
                                        <a href="#">{{$name}}</a>
                                    </div>
                                    <a href="#" class="tt-info-time">
                                        <i class="tt-icon"><svg>
                                                <use xlink:href="#icon-time"></use>
                                            </svg></i>{{ \Carbon\Carbon::CreateFromFormat('Y-m-d H:i:s', $comment->created_at)->format('d F Y') }}
                                    </a>
                                </div>
                            </div>
                            <div class="tt-item-description">
                                <div class="text-break">
                                    {{$comment->content}}
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end p-3">
                            @if (Auth::check())
                            @if (auth()->user()->id == $comment->user_id)
                                <form action="{{ route('comment.delete', ['comment_id' => $comment->id]) }}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <a href="{{ route('comment.edit', ['comment_id' => $comment->id]) }}"
                                        class="btn btn-sm btn-success">Edit</a>
                                    <input type="submit" class="btn btn-danger" value="Hapus">
                                </form>
                            @endif
                        @endif
                        </div>
                    </div> 
                @empty
                    <div class="tt-item d-flex justify-content-center my-3 bg-light p-3">
                        No Reply
                    </div>
                @endforelse
                
            </div>
        </div>
    </main>
@endsection
