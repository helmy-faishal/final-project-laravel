@extends('layout.master');
@section('judul')
    Add Post
@endsection
@section('isi')
    <div class="container">
        <div class="tt-wrapper-inner">
            <h1 class="tt-title-border">
                Create New Post
            </h1>
            <form class="form-default form-create-topic" action="{{ route('create') }}" method="POST"
                enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="inputTopicTitle">Title</label>
                    <div class="tt-value-wrapper mb-5">
                        <input type="text" name="tittle" class="form-control" id="inputTopicTitle"
                            placeholder="Subject of your topic">
                    </div>
                    @error('tittle')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Upload</span>
                        </div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" @error('image') is-invalid @enderror id="image"
                                name="image">
                            <label class="custom-file-label" for="image">Choose file</label>
                        </div>
                        @error('image')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="pt-editor">
                        <h6 class="pt-title">Content</h6>
                        <div class="form-group">
                            <textarea name="content" class="form-control" cols="120"
                                placeholder="Lets get started"></textarea>
                        </div>
                        @error('content')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="inputTopicTitle">Category</label>
                                    <select class="form-control" name="category">
                                        @forelse ($data['category'] as $key => $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @empty
                                            <option value="">None</option>
                                        @endforelse
                                    </select>
                                </div>
                                @error('category')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-auto ml-md-auto">
                                <button type="submit" class="btn btn-secondary btn-width-lg">Create Post</button>
                            </div>
                        </div>
                    </div>
            </form>
        </div>

        {{-- <script>
        $(function () {
          $("#table1").DataTable();

          Swal.fire({
              title: "Berhasil!",
              text: "Memasangkan script sweet alert",
              icon: "success",
              confirmButtonText: "Cool",
          });
        }); --}}
    @endsection
