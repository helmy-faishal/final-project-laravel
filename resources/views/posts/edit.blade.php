@extends('layout.master');
@section('judul')
    Edit Post
@endsection
@section('isi')
    <div class="container">
        <div class="tt-wrapper-inner">
            <h1 class="tt-title-border">
                Edit Post
            </h1>
            <form class="form-default form-create-topic" action="{{ route('update', ['post_id' => $data['posts']->id]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="inputTopicTitle">Tittle</label>
                    <div class="tt-value-wrapper mb-5">
                        <input type="text" name="tittle" class="form-control" id="inputTopicTitle"
                            value="{{ $data['posts']->tittle }}">
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Upload</span>
                        </div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" @error('image') is-invalid @enderror id="image"
                                name="image">
                            <label class="custom-file-label" for="image">Choose file</label>
                        </div>
                        @error('image')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                        <div class="pt-editor">
                            <h6 class="pt-title">Content</h6>
                            <div class="form-group">
                                <textarea name="content" class="form-control"
                                    cols="120">{{ $data['posts']->content }}</textarea>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="inputTopicTitle">Category</label>
                                        <select class="form-control" name="category">
                                            @forelse ($data['category'] as $key => $item)
                                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                            @empty
                                                <option value="">None</option>
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-auto ml-md-auto">
                                    <button type="submit" class="btn btn-secondary btn-width-lg">Edit Post</button>
                                </div>
                            </div>
                        </div>
            </form>
        </div>
    </div>
@endsection
