@extends('layout.master');
@section('judul')
    Edit Comment
@endsection
@section('isi')
    <div class="container">
        <div class="tt-wrapper-inner">
            <h1 class="tt-title-border">
                Edit Comment
            </h1>
            <form class="form-default form-create-topic" action="{{ route('comment.update', ['comment_id' => $data->id]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group">
                    <div class="input-group">
                        <div class="pt-editor">
                            <h6 class="pt-title">Your Comment</h6>
                            <div class="form-group">
                                <textarea name="content" class="form-control"
                                    cols="120">{{ $data->content }}</textarea>
                            </div>
                            <div class="row">
                                <div class="col-auto ml-md-auto">
                                    <button type="submit" class="btn btn-secondary btn-width-lg">Edit Comment</button>
                                </div>
                            </div>
                        </div>
            </form>
        </div>
    </div>
@endsection
