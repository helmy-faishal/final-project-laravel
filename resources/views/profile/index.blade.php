@extends('layout.master')

@section('isi')
<div class="container">
    <div class="d-flex justify-content-between my-3">
        <a class="btn btn-secondary" href="/createposts" role="button">Add New Post</a>
        <a class="btn btn-primary" href="/profile/edit" role="button">Setting</a>
    </div>
</div>

<div class="container p-2">
    <h3 class="my-3 d-flex justify-content-center">Your Posts</h3>
    <hr>
    
    <div class="tt-topic-list">
        <div class="tt-list-header">
            <div class="tt-col-topic d-flex justify-content-center">Posts</div>
            <div class="tt-col-category d-flex justify-content-center">Category</div>
            <div class="tt-col-value d-flex justify-content-center">Comments</div>
            <div class="tt-col-value d-flex justify-content-center" style="width:150px">Action</div>
        </div>
        @forelse ($data['posts'] as $key => $value)
        <div class="tt-item tt-itemselect mb-2">
            <div class="tt-col-avatar">
                <svg class="tt-icon">
                    <use xlink:href="#icon-ava-{{strtolower($value->withUser->name[0])}}"></use>
                </svg>
            </div>
            <div class="tt-col-description">
                <h6 class="tt-title"><a href="{{ route('show', ['post_id' => $value->id]) }}">
                        <svg class="tt-icon">
                            <use xlink:href="#icon-pinned"></use>
                        </svg>
                        <div class="text-break">
                            {{ $value->tittle }}
                        </div>
                    </a></h6>
                <div class="row align-items-center no-gutters">
                    <div class="col-11">
                        <ul class="tt-list-badge">
                            <li><a href="#"><span class="tt-badge">{{ $value->withUser->name }}</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="tt-col-category"><span class="tt-color01 tt-badge">{{ $value->category->name }}</span></div>
            <div class="tt-col-value tt-color-select hide-mobile">
                @php
                    echo count(App\Comment::where('posts_id',$value->id)->get());
                @endphp
            </div>
            <div class="tt-col-value hide-mobile d-flex justify-content-between" style="width:150px">
                @if (Auth::check())
                    @if (auth()->user()->id == $value->user_id)
                        <form action="{{ route('delete', ['post_id' => $value->id]) }}" method="POST">
                            @csrf
                            @method('delete')
                            <a href="{{ route('formEdit', ['post_id' => $value->id]) }}"
                                class="btn btn-sm btn-success">Edit</a>
                            <input type="submit" class="btn btn-danger" value="Hapus">
                        </form>
                    @endif
                @endif
                
            </div>
        </div>
        @empty
        <div class="row ">
            <div class="col d-flex justify-content-center bg-light">No Data</div>
        </div>
    @endforelse
</div>


<div class="container mt-5 p-2">
    <h3 class="my-3 d-flex justify-content-center">Your Comments</h3>
    <hr>
    
    <div class="tt-topic-list">
        <div class="tt-list-header">
            <div class="tt-col-topic d-flex justify-content-center">Comments</div>
            <div class="tt-col-value" style="width:150px">Action</div>
        </div>
        @forelse ($data['comments'] as $key => $value)
        @php
            $name = App\User::where('id',$value->user_id)->first()->name;
        @endphp
        <div class="tt-item tt-itemselect mb-2">
            <div class="tt-col-avatar">
                <svg class="tt-icon">
                    <use xlink:href="#icon-ava-{{strtolower($name[0])}}"></use>
                </svg>
            </div>
            <div class="tt-col-description">
                <h6 class="tt-title"><a href="{{ route('show', ['post_id' => $value->posts_id]) }}">
                        <div class="text-break">
                            {{ $value->content }}
                        </div>
                    </a></h6>
                <div class="row align-items-center no-gutters">
                    <div class="col-11">
                        <ul class="tt-list-badge">
                            <li><a href="#"><span class="tt-badge">{{ $name }}</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="tt-col-value hide-mobile d-flex justify-content-beetween" style="width:150px">
                @if (Auth::check())
                    @if (auth()->user()->id == $value->user_id)
                        <form action="{{ route('comment.delete', ['comment_id' => $value->id]) }}" method="POST">
                            @csrf
                            @method('delete')
                            <a href="{{ route('comment.edit', ['comment_id' => $value->id]) }}"
                                class="btn btn-sm btn-success">Edit</a>
                            <input type="submit" class="btn btn-danger" value="Hapus">
                        </form>
                    @endif
                @endif
                
            </div>
        </div>
        @empty
        <div class="row ">
            <div class="col d-flex justify-content-center bg-light">No Data</div>
        </div>
    @endforelse
</div>

@endsection