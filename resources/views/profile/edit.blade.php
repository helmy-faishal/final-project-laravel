@extends('layout.master')

@push('style')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
@endpush

@section('isi')
    <div class="container bg-light p-3">
        <form action="/profile" method="POST">
            @csrf
            @method('PUT')
            <div class="d-flex justify-content-center">
                <div class="tt-col-avatar">
                    <svg class="tt-icon" width="150px" height="150px">
                        <use xlink:href="#icon-ava-{{strtolower($data->name[0])}}"></use>
                    </svg>
                </div>
            </div>

            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" id="name" value="{{ old('name', $data->name) }}">
            </div>
            @error('name')
                <div class="alert alert-danger mt-1">{{ $message }}</div>
            @enderror
            <div class="form-group">
                <label for="age">Age</label>
                <input type="number" class="form-control" name="age" id="age" value="{{ old('age', $data->age) }}">
            </div>
            @error('age')
                <div class="alert alert-danger mt-1">{{ $message }}</div>
            @enderror
            <div class="form-group">
                <label for="bio">Bio</label>
                <textarea type="text" class="form-control" name="bio" id="bio" cols="30"
                    rows="10">{{ old('bio', $data->bio) }}</textarea>
            </div>
            <div class="form-group">
                <label for="address">Address</label>
                <input type="text" class="form-control" name="address" id="address"
                    value="{{ old('address', $data->address) }}">
            </div>

            <div class="d-flex justify-content-between my-3">
                {{-- <a class="btn btn-secondary" href="/profile" role="button">Back</a> --}}
                <button type="submit" class="btn btn-danger">Change</button>
            </div>

            {{-- <div class="d-flex justify-content-center my-3 mx-2">
            <a href="/logout" class="btn btn-primary">Logout</a>
        </div> --}}

        </form>
    </div>
@endsection
