@extends('layout.master')
@section('judul')
  Detail Category {{$data['category']->name}}
@endsection

@section('isi')
<div class="container">
  <h3 class="my-3 d-flex justify-content-center">Posts with category '{{$data['category']->name}}'</h3>
  <div class="col-auto ml-md-auto mt-3">
    <a href="/category/create" class="btn btn-secondary btn-width-lg mb-2">Add Category</a>
  </div>

  <hr>

  <div class="tt-topic-list">
    <div class="tt-list-header">
        <div class="tt-col-topic d-flex justify-content-center">Posts</div>
        <div class="tt-col-category">Category</div>
        <div class="tt-col-value">Comments</div>
        <div class="tt-col-value" style="width:150px"></div>
    </div>
    @foreach ($data['posts'] as $p)
        <div class="tt-item tt-itemselect mb-2" data-aos="fade-up" data-aos-anchor-placement="center-bottom">
            <div class="tt-col-avatar">
                <svg class="tt-icon">
                    <use xlink:href="#icon-ava-{{strtolower($p->withUser->name[0])}}"></use>
                </svg>
            </div>
            <div class="tt-col-description">
                <h6 class="tt-title"><a href="{{ route('show', ['post_id' => $p->id]) }}">
                        <svg class="tt-icon">
                            <use xlink:href="#icon-pinned"></use>
                        </svg>
                        <div class="text-break">
                            {{ $p->tittle }}
                        </div>
                    </a></h6>
                <div class="row align-items-center no-gutters">
                    <div class="col-11">
                        <ul class="tt-list-badge">
                            <li class="show-mobile"><a href="#"><span
                                        class="tt-color01 tt-badge">politics</span></a>
                            </li>
                            {{-- <li><a href="#"><span class="tt-badge">contests</span></a></li> --}}
                            <li><a href="#"><span class="tt-badge">{{ $p->withUser->name }}</span></a></li>
                        </ul>
                    </div>
                    <div class="col-1 ml-auto show-mobile">
                        <div class="tt-value">1h</div>
                    </div>
                </div>
            </div>
            <div class="tt-col-category"><span class="tt-color01 tt-badge">{{ $p->category->name }}</span></div>
            <div class="tt-col-value tt-color-select hide-mobile">
                @php
                    echo count(App\Comment::where('posts_id', $p->id)->get());
                @endphp
            </div>
            <div class="tt-col-value hide-mobile d-flex justify-content-between" style="width:150px">
                @if (Auth::check())
                    @if (auth()->user()->id == $p->user_id)
                        <form action="{{ route('delete', ['post_id' => $p->id]) }}" method="POST">
                            @csrf
                            @method('delete')
                            <a href="{{ route('formEdit', ['post_id' => $p->id]) }}"
                                class="btn btn-sm btn-success">Edit</a>
                            <input type="submit" id="remove" class="btn btn-danger" value="Delete">
                        </form>
                    @endif
                @endif
  
            </div>
        </div>
    @endforeach
  </div>
</div>
@endsection

@push('script')
    <script>
        $(document).ready(function() {
            AOS.init();
        })
    </script>
@endpush