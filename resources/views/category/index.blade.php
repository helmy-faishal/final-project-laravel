@extends('layout.master')
@section('judul')
  Category List
@endsection

@section('isi')

<div class="tt-wrapper-inner">
            <h1 class="tt-title-border">
                Category List
            </h1>
<div class="col-auto ml-md-auto">
    <a href="/category/create" class="btn btn-secondary btn-width-lg mb-3">Add Category</a>
</div>

@forelse ($category as $key => $item)   
<div class="col mb-3">
              <div class="card">
                <div class="card-body">
                    <h5 class="card-title"><a href="/category/{{$item->id}}"> {{$item ->name}}</h5> 
                <form action="/category/{{$item->id}}" method="POST">
                  @csrf
                    @method('delete')
                    @auth
                    <a href="/category/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    @endauth
                </form>
                </div>
              </div>
    </div>
    @empty
        <div class="col-3 mb-3">
            <p>Data is still empty</p>
        </div>
@endforelse
  
@endsection
