@extends('layout.master')
@section('judul')
  Edit Category {{$category->name}}
@endsection

@section('isi')

<div class="container">
        <div class="tt-wrapper-inner">
            <h1 class="tt-title-border">
                Edit Category
            </h1>
            <form class="form-default form-create-topic" action="/category/{{$category->id}}" method="POST">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="inputTopicTitle">Name</label>
                    <div class="tt-value-wrapper">
                        <input type="text" name="name" value="{{$category->name}}" class="form-control" id="inputCategoryName">
                    </div>
                    @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="row">
                            <div class="col-auto ml-md-auto">
                                <button type="submit" class="btn btn-secondary btn-width-lg">Update</button>
                            </div>
                    </div>
            </form>
        </div>
</div>
       
    <script>
        $(function () {
          $("#table1").DataTable();

          Swal.fire({
              title: "Berhasil!",
              text: "Memasangkan script sweet alert",
              icon: "success",
              confirmButtonText: "Cool",
          });
        });

@endsection