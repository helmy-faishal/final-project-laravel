@extends('layout.master');
@section('judul')
    Add Category
@endsection

@section('isi')
    <div class="container">
        <div class="tt-wrapper-inner">
            <h1 class="tt-title-border">
                Add Category
            </h1>
            <form class="form-default form-create-topic" action="/category" method="POST">
                @csrf
                <div class="form-group">
                    <label for="inputTopicTitle">Name</label>
                    <div class="tt-value-wrapper">
                        <input type="text" name="name" class="form-control" id="inputCategoryName"
                            placeholder="Input Category Name">
                    </div>
                    @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="row">
                        <div class="col-auto ml-md-auto">
                            <button type="submit" class="btn btn-secondary btn-width-lg">Add Category</button>
                        </div>
                    </div>
            </form>
        </div>
    </div>
    <script>
        $(function() {
            $("#table1").DataTable();

            Swal.fire({
                title: "Berhasil!",
                text: "Memasangkan script sweet alert",
                icon: "success",
                confirmButtonText: "Cool",
            });
        });
    @endsection
