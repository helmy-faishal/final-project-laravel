<?php
use App\Http\Controllers\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/post', 'homeController@home')->name('home');
Route::get('/', 'HomeController@home')->name('home');
Route::get('/post/{post_id}', 'postsController@show')->name('show');
Route::get('/createposts', 'postsController@createpost')->name('createpost')->middleware('auth');
Route::post('/create', 'postsController@create')->name('create')->middleware('auth');
Route::get('/post/{post_id}/edit', 'postsController@edit')->name('formEdit')->middleware('auth');
Route::put('/post/{post_id}', 'postsController@update')->name('update')->middleware('auth');
Route::delete('/post/{post_id}', 'postsController@delete')->name('delete')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('auth.home');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('profile.logout');

Route::get('/profile', 'profileController@index')->name('profile.index');
Route::get('/profile/edit', 'profileController@edit')->name('profile.edit');
Route::put('/profile', 'ProfileController@update')->name('profile.update');

Route::get('/category/create', 'categoryController@create')->middleware('auth');
Route::post('/category', 'categoryController@store')->middleware('auth');
Route::get('/category', 'categoryController@index')->name('category');
Route::get('/category/{category_id}', 'categoryController@show');
Route::get('/category/{category_id}/edit', 'categoryController@edit')->name('category.edit')->middleware('auth');
Route::put('/category/{category_id}', 'categoryController@update')->name('category.update')->middleware('auth');
Route::delete('/category/{category_id}', 'categoryController@destroy')->name('category.delete')->middleware('auth');

Route::post('/comment', 'commentsController@store')->name('comment.create')->middleware('auth');
Route::get('/comment/{comment_id}/edit', 'commentsController@edit')->name('comment.edit')->middleware('auth');
Route::put('/comment/{comment_id}', 'commentsController@update')->name('comment.update')->middleware('auth');
Route::delete('/comment/{comment_id}', 'commentsController@destroy')->name('comment.delete')->middleware('auth');

Route::post('/', 'HomeController@search')->name('search');
